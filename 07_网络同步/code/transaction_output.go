package main

import (
	"bytes"
	"encoding/gob"
	"log"
)

//输出
type TXOutput struct {
	Value int	//output保存了“币”（上面额value）
	PubKeyHash []byte		//用脚本语言意味着比特币可以也作为智能合约平台
}
//输出锁住的标志
func (out *TXOutput)Lock(address []byte)  {
	pubkeyhash := Base58Decode(address)
	pubkeyhash=pubkeyhash[1:len(pubkeyhash)-4]		//截取有效哈希
	out.PubKeyHash = pubkeyhash		//锁住，无法再被修改
}
//检测是否被key锁住
func (out *TXOutput)isLockedWithKey(pubKeyHash []byte) bool {
	return bytes.Compare(out.PubKeyHash,pubKeyHash) == 0
}

//创造一个输出
func NewTXOutput(value int,address string)*TXOutput{
	txo := &TXOutput{Value:value,PubKeyHash:nil}
	txo.Lock([]byte(address))		//锁住
	return txo
}

type TXoutputs struct {
	Outputs [] TXOutput
}
// 对象转化为二进制字节集
func (out *TXoutputs) Serialize() []byte {
	var buff bytes.Buffer		// 开辟内存，存放字节集合
	encoder := gob.NewEncoder(&buff)	// 编码对象创建
	err := encoder.Encode(out)	//编码操作
	if err!=nil{
		log.Panic(err)	//处理错误
	}
	return buff.Bytes()	//返回字节
}

// 读取文件，二进制字节集转化为对象
func DeserializeOutputs(data []byte) *TXoutputs {
	var outputs TXoutputs //对象存储用于字节转化的对象
	decoder := gob.NewDecoder(bytes.NewReader(data))	//解码
	err := decoder.Decode(&outputs)		//尝试解码
	if err!=nil{
		log.Panic(err)	//处理错误
	}
	return &outputs
}

