package main

import (
	"bytes"
)

//输入
type TXInput struct {
	Txid []byte		//Txid存储了交易的id
	Vout int		//Vout则保存该交易中一个output索引
	Signature []byte	//签名
	Pubkey []byte	//公钥
}

//key检测一下地址和交易
func (in *TXInput)UsesKey(pubKeyHash []byte) bool {
	lockinghash := HashPubkey(in.Pubkey)
	return bytes.Compare(lockinghash,pubKeyHash) == 0
}

