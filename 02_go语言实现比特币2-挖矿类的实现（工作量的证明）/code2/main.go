package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println("hello game start")
	bc := NewBlockchain() //创建区块链
	bc.AddBlock("张亚飞1 pay 小花 10")
	bc.AddBlock("张亚飞2 pay 小花 20")
	bc.AddBlock("张亚飞3 pay 小花 30")
	for _, block := range bc.blocks {
		fmt.Printf("上一块哈希%x\t", block.PrevBlockHash)
		fmt.Printf("数据：%s\t", block.Data)
		fmt.Printf("当前哈希%x\t", block.Hash)
		pow:=NewProofOfWork(block)	//校验工作量
		fmt.Printf("pow %s\n",strconv.FormatBool(pow.Validate()))
		fmt.Println()
	}
}
